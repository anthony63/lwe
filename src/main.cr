require "./engine/window"

require "crystglfw"
include CrystGLFW

TITLE = "LWE Engine | BETA 0.0.1"
WIDTH = 1280
HEIGHT = 720

CrystGLFW.run do
    win = LWEWindow.new(TITLE, WIDTH, HEIGHT)
    win.run
end
