require "../engine/shader_program"
require "../math/glm"
require "../math/math"
require "../entities/camera"

class LWEStaticShader < LWEShaderProgram

    @VERTEX_FILE_PATH = "shaders/shader.vert"
    @FRAGMENT_FILE_PATH = "shaders/shader.frag"

    property transform_matrix_location : Int32 = 0
    property projection_matrix_location : Int32 = 0
    property view_matrix_location : Int32 = 0
    def initialize
        super(@VERTEX_FILE_PATH, @FRAGMENT_FILE_PATH)
    end

    def get_all_uniform_locations
        @transform_matrix_location = get_uniform_location("transform_matrix")
        @projection_matrix_location = get_uniform_location("projection_matrix")
        @view_matrix_location = get_uniform_location("view_matrix")
    end

    def bind_attributes
        bind_attribute(0, "position")
        bind_attribute(1, "tex_coords")
    end

    def load_transform_matrix(matrix : GLM::Mat4)
        load_matrix(@transform_matrix_location, matrix)
    end

    def load_projection_matrix(matrix : GLM::Mat4)
        load_matrix(@projection_matrix_location, matrix) 
    end

    def load_view_matrix(camera : LWECamera)
        matrix = LWEMath.create_view_matrix(camera)
        load_matrix(@view_matrix_location, matrix)
    end
end