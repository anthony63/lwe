require "../math/glm"
require "crystglfw"

include CrystGLFW

class LWECamera
    property position : GLM::Vec3
    property pitch : Float32
    property yaw : Float32
    property roll : Float32
    property win : Window 
    
    def initialize(win : Window)
        @position = GLM.vec3(0.0f32,0.0f32,-2.0f32);
        @win = win
        @pitch = 0.0
        @yaw = 0.0
        @roll = 0.0
    end

    def move
        if @win.key_pressed?(Key::W)
            @position.z -= 0.02f32
        end
        if @win.key_pressed?(Key::S)
            @position.z += 0.02f32
        end
        if @win.key_pressed?(Key::D)
            @position.x += 0.02f32
        end
        if @win.key_pressed?(Key::A)
            position.x -= 0.02f32
        end
        if @win.key_pressed?(Key::Space)
            position.y += 0.02f32
        end
        if @win.key_pressed?(Key::LeftShift)
            position.y -= 0.02f32
        end

    end
end