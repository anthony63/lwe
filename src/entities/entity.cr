require "../models/textured_model"
require "../math/glm"

class LWEEntity
    property model : LWETexturedModel
    property position : GLM::Vec3
    property rot_x : Float32
    property rot_y : Float32
    property rot_z : Float32
    property scale : Float32
    
    def initialize(model : LWETexturedModel, position : GLM::Vec3, rot_x : Float32, rot_y : Float32, rot_z : Float32, scale : Float32)
        @model = model 
        @position = position 
        @rot_x = rot_x 
        @rot_y = rot_y 
        @rot_z = rot_z 
        @scale = scale 
    end

    def pos_add(dx : Float32, dy : Float32, dz : Float32)
        @position.x += dx
        @position.y += dy
        @position.z += dz
    end

    def rot_add(rx : Float32, ry : Float32, rz : Float32)
        @rot_x += rx
        @rot_y += ry
        @rot_z += rz
    end
end