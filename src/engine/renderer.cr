require "lib_gl"

require "../entities/entity"
require "../shaders/static_shader"
require "../math/math"
require "../math/glm"

class LWERenderer

    @FOV = 70.0
    @NEAR_PLANE = 0.1f32
    @FAR_PLANE = 1000.0f32
    property projection_matrix : GLM::Mat4
    def initialize(shader : LWEStaticShader, width, height)
        @projection_matrix = GLM.perspective(@FOV,width / height,@NEAR_PLANE, @FAR_PLANE)
        shader.start
        shader.load_projection_matrix(@projection_matrix)
        shader.stop
        LibGL.clear_color(0.325, 0.271, 0.51, 1.0)
    end


    def render(entity : LWEEntity, shader : LWEStaticShader)
        textured_model = entity.model
        model = textured_model.raw_model

        LibGL.enable(LibGL::DEPTH_TEST)
        LibGL.clear(LibGL::COLOR_BUFFER_BIT | LibGL::DEPTH_BUFFER_BIT)

        LibGL.bind_vertex_array(model.vao_id)

        LibGL.enable_vertex_attrib_array(0)
        LibGL.enable_vertex_attrib_array(1)

        transform_matrix = LWEMath.create_transform_matrix(entity.position, entity.rot_x, entity.rot_y, entity.rot_z, entity.scale)
        shader.load_transform_matrix(transform_matrix)

        LibGL.active_texture(LibGL::TEXTURE0)
        LibGL.bind_texture(LibGL::TEXTURE_2D, textured_model.texture.id)

        LibGL.draw_elements(LibGL::TRIANGLES, model.vertex_count, LibGL::UNSIGNED_INT, Pointer(Void).new(0))

        LibGL.disable_vertex_attrib_array(0)
        LibGL.disable_vertex_attrib_array(0)

        LibGL.bind_vertex_array(0)
    end
end