require "lib_gl"
require "../math/glm"
abstract class LWEShaderProgram
    property vertex_shader_id : UInt32, fragment_shader_id : UInt32, program_id : UInt32
    
    def initialize(vertex_file : String, fragment_file : String)
        @vertex_shader_id = load_shader(vertex_file, LibGL::VERTEX_SHADER)
        @fragment_shader_id = load_shader(fragment_file, LibGL::FRAGMENT_SHADER)
        @program_id = LibGL.create_program;
        LibGL.attach_shader(@program_id, @vertex_shader_id)
        LibGL.attach_shader(@program_id, @fragment_shader_id)
        bind_attributes
        LibGL.link_program(@program_id)
        LibGL.validate_program(@program_id)  
        get_all_uniform_locations
    end

    abstract def get_all_uniform_locations
    abstract def bind_attributes

    def get_uniform_location(uniform_name : String)
        return LibGL.get_uniform_location(@program_id, uniform_name)
    end

    def bind_attribute(attribute : Int32, variable_name : String)
        puts "ShaderProgram(#{@program_id}):\n\tAttribute: #{attribute}\n\tName #{variable_name}"
        LibGL.bind_attrib_location(@program_id, attribute, variable_name)
    end

    def load_float(location : Int32, value : Float32)
        puts "ShaderProgram(#{@program_id}):\n\tLocation: #{location}\n\tValue: #{value}"
        LibGL.uniform_1f(location, value)
    end

    def load_vector(location : Int32, value : GLM::Vec3)
        puts "ShaderProgram(#{@program_id}):\n\tLocation: #{location}\n\tValue(VECTOR): (#{value.x}, #{value.y}, #{value.z})"
        LibGL.uniform_3f(location, value.x, value.y, value.z)
    end

    def load_bool(location : Int32, value : Bool)
        load = 0.0
        if value == true
            load = 1.0
        end

        LibGL.uniform_1f(location, load)
    end

    def load_matrix(location : Int32, value : GLM::Mat4)
        LibGL.uniform_matrix_4fv(location, 1, LibGL::FALSE, value.buffer)
    end

    def start()
        LibGL.use_program(@program_id)
    end

    def stop
        LibGL.use_program(0)
    end


    def cleanup()
        stop()
        LibGL.detach_shader(@program_id, @vertex_shader_id)
        LibGL.detach_shader(@program_id, @fragment_shader_id)
        LibGL.delete_shader(@vertex_shader_id)
        LibGL.delete_shader(@fragment_shader_id)
        LibGL.delete_program(@program_id)
    end

    private def load_shader(file : String, type : UInt32) : UInt32
        shader_id = LibGL.create_shader(type)
        if shader_id == 0
            shader_error_code = LibGL.get_error
            raise "Error #{shader_error_code}: Shader creation failed. Could not find valid memory location when adding shader"
        end
        
        text = File.read_lines(file).join("\n")
        ptr = text.to_unsafe
        source = [ptr]
        LibGL.shader_source(shader_id, 1, source, Pointer(Int32).new(0))
        LibGL.compile_shader(shader_id)
        
        LibGL.get_shader_iv(shader_id, LibGL::COMPILE_STATUS, out compile_status)
        if compile_status == LibGL::FALSE
            LibGL.get_shader_info_log(shader_id, 2048, nil, out compile_log)
            compile_log_str = String.new(pointerof(compile_log))
            compile_error_code = LibGL.get_error
            raise "Error #{compile_error_code}: Failed compiling shader.\n'#{text}': #{compile_log_str}"
        end
        shader_id
    end
end