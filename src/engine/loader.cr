require "../models/raw_model"
require "../util/bitmap"
require "lib_gl"

class LWELoader
    def self.load_to_vao(positions :  Array(Float32), texture_coords : Array(Float32), indices : Array(Int32)) : LWERawModel
        vao_id = create_vao
        bind_indices_buffer(indices)
        store_data_in_attrib_list(0, 3, positions)
        store_data_in_attrib_list(1, 2, texture_coords)
        unbind_vao
        LWERawModel.new(vao_id, indices.size)
    end
    
    def self.create_vao
        LibGL.gen_vertex_arrays(1, out vao_id)
        LibGL.bind_vertex_array(vao_id)
        puts "VAO(#{vao_id})"
        vao_id
    end

    def self.load_texture(filename : String)
        if File.exists?(filename) == false
            puts "Texture: FILE NOT FOUND\n\tFile name: #{filename}"
            exit -1
        end

        bitmap = Bitmap.new(filename)

        LibGL.gen_textures(1, out texture_id)
        LibGL.bind_texture(LibGL::TEXTURE_2D, texture_id)

        if bitmap.pixels.size > 0
            LibGL.tex_parameter_i(LibGL::TEXTURE_2D, LibGL::TEXTURE_WRAP_S, LibGL::REPEAT)
            LibGL.tex_parameter_i(LibGL::TEXTURE_2D, LibGL::TEXTURE_WRAP_T, LibGL::REPEAT)

            LibGL.tex_parameter_i(LibGL::TEXTURE_2D, LibGL::TEXTURE_MIN_FILTER, LibGL::NEAREST)
            LibGL.tex_parameter_i(LibGL::TEXTURE_2D, LibGL::TEXTURE_MAG_FILTER, LibGL::NEAREST)

            format = bitmap.alpha? ? LibGL::RGBA : LibGL::RGB

            LibGL.tex_image_2d(LibGL::TEXTURE_2D, 0, format, bitmap.width, bitmap.height, 0, format, LibGL::UNSIGNED_BYTE, bitmap.pixels)
            LibGL.generate_mipmap(LibGL::TEXTURE_2D)
            LibGL.tex_parameter_i(LibGL::TEXTURE_2D, LibGL::TEXTURE_MIN_FILTER, LibGL::LINEAR_MIPMAP_LINEAR)
            LibGL.tex_parameter_f(LibGL::TEXTURE_2D, LibGL::TEXTURE_LOD_BIAS, -0.4)
            LibGL.bind_texture(LibGL::TEXTURE_2D, 0)
        else
            puts "Failed to load bitmap #{filename}"
            exit
        end
        return texture_id
    end

    private def self.store_data_in_attrib_list(attribute : Int32, size : Int32, data : Array(Float32))
        LibGL.gen_buffers(1, out vbo_id)
        puts "VBO(#{vbo_id}):\n\tAttribute: #{attribute}\n\tData: #{data}"
        LibGL.bind_buffer(LibGL::ARRAY_BUFFER, vbo_id)
        LibGL.buffer_data(LibGL::ARRAY_BUFFER, data.size * sizeof(Float32), data, LibGL::STATIC_DRAW)
        LibGL.vertex_attrib_pointer(attribute, size, LibGL::FLOAT, LibGL::FALSE, 0, Pointer(Void).new(0))
        LibGL.bind_buffer(LibGL::ARRAY_BUFFER, 0)
        vbo_id
    end

    private def self.unbind_vao
        LibGL.bind_vertex_array(0)
    end

    private def self.bind_indices_buffer(data : Array(Int32))
        LibGL.gen_buffers(1, out ebo_id)
        puts "EBO(#{ebo_id})\n\tData: #{data}"
        LibGL.bind_buffer(LibGL::ELEMENT_ARRAY_BUFFER, ebo_id)
        LibGL.buffer_data(LibGL::ELEMENT_ARRAY_BUFFER, data.size * sizeof(Int32), data, LibGL::STATIC_DRAW)
    end
end