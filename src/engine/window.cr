require "lib_gl"
require "crystglfw"

require "./loader"
require "./renderer"
require "../math/glm"
require "./shader_program"
require "../entities/entity"
require "../models/raw_model"
require "../models/textured_model"
require "../shaders/static_shader"
require "../textures/model_texture"
require "../model_data/cube"

include CrystGLFW

class LWEWindow
    property renderer : LWERenderer
    property shader : LWEShaderProgram
    property win : CrystGLFW::Window
    property static_model : LWETexturedModel
    property entity : LWEEntity
    property camera : LWECamera

    def initialize(title : String, width : Int32, height : Int32)
        hints = {
            Window::HintLabel::ContextVersionMajor => 3,
            Window::HintLabel::ContextVersionMinor => 3,
            Window::HintLabel::ClientAPI => ClientAPI::OpenGL,
            Window::HintLabel::OpenGLProfile => OpenGLProfile::Core,
            Window::HintLabel::Resizable => false,
        }
        @win = Window.new(title: title, width: width, height: height, hints: hints)
        @win.make_context_current

        LibGL.viewport(0, 0, width, height)
        
        model = LWELoader.load_to_vao(LWECube::VERTICES, LWECube::TEXTURE_COORDS, LWECube::INDICES)
        @static_model = LWETexturedModel.new(model, LWEModelTexture.new(LWELoader.load_texture("assets/kerone.png")))
        @shader = LWEStaticShader.new
        @renderer = LWERenderer.new(@shader, width, height)
        @entity = LWEEntity.new(@static_model, GLM.vec3(0, 0, -5), 0, 0, 0, 1)
        @camera = LWECamera.new(@win)
    end
    
    def run
        until @win.should_close?
            CrystGLFW.poll_events
            if @win.key_pressed?(Key::Escape)
                @win.should_close
            end

            update
            render

            @win.swap_buffers
        end
        destroy
    end

    private def update
        @camera.move
    end

    private def render
        @shader.start
        @shader.load_view_matrix(@camera)
        @renderer.render(@entity, @shader)
        @shader.stop
    end

    def destroy
        @win.destroy
    end
end