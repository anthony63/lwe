require "./glm"
require "../entities/camera"
class LWEMath
    def self.create_transform_matrix(translation : GLM::Vec3, rx : Float32, ry : Float32, rz : Float32, scale : Float32)
        mat = GLM.translate(translation)
        rot = GLM.rotation(GLM.vec3(GLM.deg_to_rad(rx), GLM.deg_to_rad(ry), GLM.deg_to_rad(rz)))
        sc = GLM.scale(GLM.vec3(scale, scale, scale))
        mat = rot * sc * mat
        mat
    end

    def self.create_view_matrix(camera : LWECamera)
        rot = GLM.rotation(GLM.vec3(GLM.deg_to_rad(camera.pitch), GLM.deg_to_rad(camera.yaw), 0.0))
        mat = GLM.translate(GLM.vec3(-camera.position.x, -camera.position.y, -camera.position.z))
        mat = rot * mat
        mat
    end
end