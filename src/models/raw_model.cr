class LWERawModel
    property vao_id, vertex_count
    def initialize(vao_id : UInt32, vertex_count : Int32)
        puts "Model:\n\tVao ID: #{vao_id}\n\tVertex Count: #{vertex_count}"
        @vao_id = vao_id
        @vertex_count = vertex_count
    end
end