require "./raw_model"
require "../textures/model_texture"
class LWETexturedModel
    property raw_model : LWERawModel
    property texture : LWEModelTexture

    def initialize(raw_model : LWERawModel, texture : LWEModelTexture)
        @raw_model = raw_model
        @texture = texture
    end
end