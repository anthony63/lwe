#version 400 core

in vec3 position;
in vec2 tex_coords;

out vec3 color;
out vec2 tex_coords_pass;

uniform mat4 transform_matrix;
uniform mat4 projection_matrix;
uniform mat4 view_matrix;

void main() {
    gl_Position = projection_matrix * view_matrix * transform_matrix * vec4(position, 1.0);
    // gl_Position = projection_matrix * transform_matrix * vec4(position, 1.0);
    tex_coords_pass = tex_coords;
    color = vec3(position.x + 0.5, 0.0, position.y + 0.5);
}