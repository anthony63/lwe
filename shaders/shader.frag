#version 400 core

in vec3 color;
in vec2 tex_coords_pass;

out vec4 out_color;

uniform sampler2D tex_sampler;

void main() {
    out_color = texture(tex_sampler, tex_coords_pass);
}